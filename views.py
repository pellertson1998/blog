from blog.models import Entry
from django.contrib.syndication.views import Feed
from django.shortcuts import render
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.views import View
from django.utils.feedgenerator import Atom1Feed
from jsonfeed import JSONFeed

class EntryView(DetailView):
	""" Class-based view for Entry models. """
	model = Entry
	context_object_name = "entry"
	template_name = "post.html"


class BaseEntryIndex(ListView):
	"""
	Base class containing all of the variables that
	each IndexView shares.
	"""
	model = Entry
	context_object_name = "entries"
	template_name = "list.html"


class LatestEntryIndexView(BaseEntryIndex):
	"""
	Provides one big index for all Entries.

	Might reduce this to only show a year’s worth of entries if
	there are too many entries.
	"""

	def get_queryset(self):
		return Entry.objects.order_by('-date_published')[0:5]


class MonthEntryIndexView(BaseEntryIndex):
	""" Gets all entries in a given month. """

	def get_queryset(self):
		return Entry.objects.filter(
			date_published__year=self.kwargs['year'],
			date_published__month=self.kwargs['month']
		)


class YearEntryIndexView(BaseEntryIndex):
	""" Gets all entries in a given year. """

	def get_queryset(self):
		return Entry.objects.filter(
			date_published__year=self.kwargs['year'],
		)


class EntryRssFeed(Feed):
	# TODO: make the following class variables environment variables
	title = 'Latest from Parker Ellertson'
	link = '/blog/'
	description = 'Wise words from a man who knows how to ski.'
	feed_url = '/blog/feed/'
	feed_copyright = 'CC-BY (https://creativecommons.org/licenses/by/4.0/)'

	def items(self):
		return Entry.objects.order_by('-date_published')[0:5]

	def item_title(self, item):
		return item.title

	def item_link(self, item):
		return item.href

	def item_description(self, item):
		return item.post

	def item_pubdate(self, item):
		return item.date_published

	def item_updateddate(self, item):
		return item.date_last_modified

	def item_copyright(self):
		return self.feed_copyright


class EntryAtomFeed(EntryRssFeed):
	feed_type = Atom1Feed
	subtitle = EntryRssFeed.description


class EntryJsonFeed(EntryRssFeed):
	feed_type = JSONFeed