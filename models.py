import markdown2

from django.db import models
from django.contrib.postgres.fields import ArrayField


class Entry(models.Model):
	title = models.CharField(max_length=30)
	date_published = models.DateTimeField(auto_now_add=True)
	date_last_modified = models.DateTimeField(auto_now=True)
	tags = ArrayField(
		models.CharField(max_length=12, blank=False),
		size=12)
	post_md = models.TextField()

	@property
	def post(self):
		""" Compiles the post from markdown into HTML. """
		return markdown2.markdown(self.post_md)

	@property
	def href(self):
		"""
		Return a URL for the blog post.
		"""
		return f"/blog/post/{self.id}/"




