Parker's Blog System
====================
This was an exercise in learning how to use Django properly.  Plus I always
wanted a blog on the internet that I made myself.  Now that it’s at a point
where one could call it complete, I want to share it with the world.

FEATURES
--------
* Entry model to hold the blog posts.
	* Includes a Title, Post, and two date fields for creation and last modification.

* Beautiful URLs for the blog posts.
* A view for looking at a list of blog posts. URLs include:
	* `/blog/` for all posts.
	* `/blog/<year>/` for all posts in one year.
	* `/blog/<year>/<month>/` for all posts in a calendar month.
	* `/blog/post/<id>/` for each individual post.

* Posts are written in markdown and are rendered properly!
* Tags for posts (they do nothing for now but I put them there for a reason).
* RSS/Atom/JSON feeds for the blog.

REQUIREMENTS
------------
* The Entry model requires a Postgres database.
* The Python requirements are listed in [here](/requirements.txt).

INSTALL
-------
1. Install the requirements located in [requirements.txt](/requirements.txt).  Manually or with `pip` is okay.
2. Run `python manage.py test blog` to make sure everything is working properly.
3. Add the following line to your `INSTALLED_APPS` setting:

```python
INSTALLED_APPS = [
    # ...
    'blog.apps.BlogConfig',
    # ...
]
```

4. Include the blog URLconf in your urls.py file like so:

    path('blog/', include('blog.urls')),

5. Include the templates in your TEMPLATES setting:

```python
TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [
            # ...
            os.path.join(BASE_DIR, 'blog/templates'),
            # ...
        ],
        # ...
    }
]
```

6. Run `python manage.py migrate` to make the blog models.
7. Start a development server and visit http://127.0.0.1:8000/admin/ to add an entry.
8. Visit http://127.0.0.1:8000/blog/ and enjoy your new blog!