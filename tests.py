from blog.models import Entry
from blog.views import LatestEntryIndexView, MonthEntryIndexView, YearEntryIndexView
from datetime import datetime
from django.test import TestCase
from html.parser import HTMLParser


# simple HTML parser for the test cases because I didn't want to
# have BeautifulSoup as a dependency when it'd only be used here
class TestHTMLParser(HTMLParser):
	tags = {}
	start_counting = False

	def handle_starttag(self, tag, attrs):
		"""
		This function constructs a dictionary consisting of
		the keys being the html tags and the values being how
		many times the tags show up in the stream of HTML parsed
		in.

		HOWEVER, we only care about the tags that are inside a very
		specific HTML tag; the <ul> tag with an id of "entries".
		So counting the tags only begins once we reach that tag, and
		ends once we're out of it.
		"""
		attrs_dict = dict(attrs)
		if tag == 'ul':
			if attrs_dict['id'] == 'entries':
				self.start_counting = True

		if self.start_counting:
			try:
				self.tags[tag] += 1
			except KeyError:
				self.tags[tag] = 1


	def handle_endtag(self, tag):
		"""
		Only turn off the counting once we're outside of the
		<ul> tag that we care about.
		"""
		if tag == 'ul' and self.start_counting:
			self.start_counting = False


class EntryModelTests(TestCase):

	def setUp(self):
		Entry.objects.create(
			title='Test 1',
			tags=['foo','bar'],
			post_md="Normal text."
		)

		Entry.objects.create(
			title='Test 2',
			tags=['spam','eggs'],
			post_md="**bold text.**"
		)

		Entry.objects.create(
			title='Test 3',
			tags=['spam','eggs'],
			post_md="_italic text._"
		)

	def test_entry_returns_valid_html(self):
		t1 = Entry.objects.get(title='Test 1')
		self.assertEqual(t1.post, "<p>Normal text.</p>\n")

		t2 = Entry.objects.get(title='Test 2')
		self.assertEqual(t2.post, "<p><strong>bold text.</strong></p>\n")

		t3 = Entry.objects.get(title='Test 3')
		self.assertEqual(t3.post, "<p><em>italic text.</em></p>\n")


class EntryViewTests(TestCase):
	time_fmt = "%Y/%m/%d %H:%M:%S %Z"

	def setUp(self):
		Entry.objects.create(
			title='Test 1',
			tags=['foo','bar'],
			post_md="Normal text.",

		)
		e = Entry.objects.get(title="Test 1")
		e.date_published = datetime.strptime("2019/01/01 00:00:00 UTC", self.time_fmt)
		e.save()

		Entry.objects.create(
			title='Test 2',
			tags=['spam','eggs'],
			post_md="**bold text.**",
		)

		e = Entry.objects.get(title="Test 2")
		e.date_published = datetime.strptime("2018/01/01 00:00:00 UTC", self.time_fmt)
		e.save()

		Entry.objects.create(
			title='Test 3',
			tags=['spam','eggs'],
			post_md="_italic text._",
		)

		e = Entry.objects.get(title="Test 3")
		e.date_published = datetime.strptime("2019/02/01 00:00:00 UTC", self.time_fmt)
		e.save()

	def test_list_all_blog_posts(self):
		# get the view and test the response code before
		# any other testing happens
		response = self.client.get('/blog/')
		self.assertEqual(response.status_code, 200)

		# now we feed the data into the parser
		parser = TestHTMLParser()
		parser.feed(str(response.content))

		# time to test
		self.assertEqual(parser.tags['ul'], 1, "Something wrong with the HTML parser.")
		self.assertEqual(parser.tags['li'], 3)
		self.assertEqual(parser.tags['a'],  3)
		parser.tags.clear()

	def test_list_only_posts_from_2019(self):
		# get the view and test the response code before
		# any other testing happens
		response = self.client.get('/blog/2019/')
		self.assertEqual(response.status_code, 200)

		# now we feed the data into the parser
		parser = TestHTMLParser()
		parser.feed(str(response.content))

		# time to test
		self.assertEqual(parser.tags['ul'], 1, "Something wrong with the HTML parser.")
		#import pdb; pdb.set_trace()
		self.assertEqual(parser.tags['li'], 2, "HTML: " + str(response.content))
		self.assertEqual(parser.tags['a'],  2)
		parser.tags.clear()

	def test_list_only_posts_from_2019_in_january(self):
		# get the view and test the response code before
		# any other testing happens
		response = self.client.get('/blog/2019/1/')
		self.assertEqual(response.status_code, 200)

		# now we feed the data into the parser
		parser = TestHTMLParser()
		parser.feed(str(response.content))

		# time to test
		self.assertEqual(parser.tags['ul'], 1, "Something wrong with the HTML parser.")
		self.assertEqual(parser.tags['li'], 1, "HTML: " + str(response.content))
		self.assertEqual(parser.tags['a'],  1)
		parser.tags.clear()
