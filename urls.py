from django.urls import path
from .views import (
	EntryView,
	EntryAtomFeed,
	EntryJsonFeed,
	EntryRssFeed,
	LatestEntryIndexView,
	MonthEntryIndexView,
	YearEntryIndexView,
)

urlpatterns = [
	# essentially stolen from:
	# https://docs.djangoproject.com/en/2.2/topics/http/urls/#example
	# path('', EntryView.as_view()),
	path('', LatestEntryIndexView.as_view()),
	path('<int:year>/', YearEntryIndexView.as_view()),
	path('<int:year>/<int:month>/', MonthEntryIndexView.as_view()),
	path('post/<int:pk>/', EntryView.as_view()),

	# feeds
	path('feed/', EntryRssFeed()),
	path('feed/rss', EntryRssFeed()),
	path('feed/atom', EntryAtomFeed()),
	path('feed/json', EntryJsonFeed()),
]